const myCloud = uniCloud.init({
	provider: 'aliyun',
	spaceId: '7af55062-b5c5-47c7-846f-fe4411621f01',
	clientSecret: 'xwr49GEuqON85eq3cHCLDg=='
});

module.exports = {
	callFunction: (name, _data, callback,tool={"showLoading":false}) => {
		if(tool.showLoading){
			uni.showLoading({
				title:tool.showLoading,
			})
		}
		//加上token
		const user_info = uni.getStorageSync('user_info');
		console.log(user_info);
		myCloud.callFunction({
				name: name,
				data: {
					token: user_info.token || 123,
					uid: user_info._id || 123,
					data: _data
				}
			})
			.then(e => {
				if(tool.showLoading){uni.hideLoading()}
				const res = e.result
				console.log(res);
				if (res.code <= 1111 && e.success) {
					callback(res.data, res, null)
				}
				if (!e.success) {
					uni.showModal({
						content: '服务器异常请稍后再试',
						showCancel: false
					});
					return false
				}
				switch (res.code) {
					case 1111:
						user_info.token = res.token
						uni.setStorage({
							key: 'user_info',
							data: user_info,
							success: function() {
								console.log('已经缓存')
							}
						});
						break;
					case 4001:
						uni.reLaunch({
							url: '/pages/login/login'
						})
						break;
					case 4003:
						uni.showToast({
							title: '没有权限访问，自动返回！',
							icon: 'none',
							success() {
								uni.switchTab({
									url:'/pages/disinfect/list/list'
								})
							}
						});
						
						break;
					default:
						break;
				}
			});
	},
	uploadFile: (path, callback) => {
		uni.showLoading();
		myCloud.uploadFile({
			filePath: path,
			success: (res) => {
				callback(res);
			},
			fail: (err) => {
				console.log(err);
			},
			complete: () => {
				uni.hideLoading();
			}
		})
	},
	deleteFile: (path, callback) => {
		uni.showLoading();
		myCloud.deleteFile({
			fileList: [path],
			success: (res) => {
				callback(res);
			},
			fail: (err) => {
				console.log(err);
			},
			complete: () => {
				uni.hideLoading();
			}
		})
	}
}
